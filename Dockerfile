# Dockerfile
# Establecemos que imagen utilizar
FROM ubuntu:latest

# LABELS
LABEL maitainer="Vinicius Gomes"
LABEL version="2.0"
LABEL description="TP2a - Registro de alumnos con un sitio estático de INTRANET"

# VARIABLES PARA PASAR ARGUMENTO 
ARG DEBIAN_FRONTEND=noninteractive
 
# Instalacion y configuracion de Stack Lamp
RUN apt update && apt install -y wget apache2 mysql-server php libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip && apt autoremove && apt clean

# Variables de entorno para el ServerName y ServerApp 
ENV APPSERVERNAME registro.istea
ENV APPALIAS www.registro.istea

# Variables de entorno para el mysql
ENV MYSQL_USER isteauser
ENV MYSQL_USER_PASSWORD 1234
ENV MYSQL_DB_NAME isteadb
 
# Creamos el directorio 'app' en el raiz
RUN mkdir -p /app

# Copiar webpages al directorio 'app'
COPY materias_registradas.php /app
COPY index.php /app

# Dar permisos de lectura a los paginas we
RUN chmod +r /app/materias_registradas.php
RUN chmod +r /app/index.php

# Copiamos el fichero de vhost a 'sites-available'
COPY default.conf /etc/apache2/sites-available

# Exponemos el puerto 80
EXPOSE 80


# Copiamos el directorio 'apache2' de 'etc' hacia '/app/apache2'
# Si hay un punto de montaje hacia el directorio 'apache2'
# El entrypoint al encontrar vacio al directorio va a traer los ficheros 
# correspondientes de '/app/apache2'
RUN cp -r /etc/apache2 /app/apache2

# Definicion de los volumenes (para paginas, librerias y cofniguraciones de apache) 
VOLUME ["/var/www/html", "/var/lib/mysql", "/etc/apache2"]

# ENTRYPOINT 
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
