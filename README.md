# TUTORIAL



## Proyecto de Instalación de Docker LAMP para sitio Estático de INTRANET

Elegimos hostear un sitio estátio en Apache2, para registro de materias que serán cursadas por un estudiante de ISTEA. 
En la página principal se registran las materias y en la siguiente se veen las materias registradas. Además hay un boton de redireccionamiento a sitio principal de www.istea.com.ar. 

nota: El mysql será iniciado para demonstrar su funcionamento pero no lo utilizaremos para este projecto. Los datos serán pasados de una página a otra por medio del POST.

## Instrucciones para la creación de una imagendocker
1. Ingreamos a una terminal linux bash. 

2. Bajamos los fichores de nuestro proyecto: 
```
$ git clone https://gitlab.com/vinistea/actualizacion.git  
```
3. Entramos a la carpeta de bajada (actualizacion) y creamos de una imagem Docker.
```
$ cd actualizacion
$ docker build -t proxyimage .
```

### Para ingresar a la página usando un proxy inverso con NGINX.

El primer paso es correr el contedor del servidor inverso
```
docker run -d -p 80:80 \
-v /var/run/docker.sock:/tmp/docker.sock:ro \
jwilder/nginx-proxy
```

Luego corremos el contedor con el siguiente comando
```
$ docker run -d --name regconte \
-e APPSERVERNAME=registro.istea \
-e APPALIAS=www.registro.istea \
-e MYSQL_USER_PASSWORD=1234 \
-e MYSQL_DB_NAME=isteadb \
-e VIRTUAL_HOST=registro.istea \
-v /tmp/registroistea/www:/var/www/html \
proxyimage
```
**Opciones:**
<br>`-d`: lo deja en segundo plano
<br>`--name`: para elegir el nombre del contenedor (registrocontendedor)
<br>`-e`: para pasar las variables de entorno
<br>`-v`: bind de montaje de un volumen
<br>`-p`: mapeamento de puertos (<host port>:<contenedor port>)


Para acceder al sitio hay que agregar el host `registro.istea`. En Windows lo encontrás en `C:\Windows\System32\drivers\etc\host`, y en linux debia está en `/etc/hosts`.

Luego, desde un navegador tipeás `http://registro.istea`.

Listooo!
